import React from "react"
import axios from "axios";
import BlodTable from './bloodDocShow'

export default class BloodShowcase extends React.Component{
    
    constructor(props) {
        super(props);
        this.state = {listOfBlod: []};  
      }
      componentDidMount(){  
        debugger;  
        axios.get('https://localhost:44335/Api/Blood/BloodDetails')  
          .then(response => {  
            this.setState({ listOfBlod: response.data });  
            debugger; 
  
          })  
          .catch(function (error) {  
            console.log(error);  
          })  
      }  
  
      blodAdminRow(){  
        return this.state.listOfBlod.map(function(object, i){  
            return <BlodTable obj={object} key={i} />;  
        });  
      }

    render(){
    return(
    <div id="showcaseform">
      <h4 id="headerTitle">Blood Entry and Exit</h4>
          <table className="table table-striped" style={{ marginTop: 10 }}>  
            <thead>  
              <tr style={{ textAlign: "center" }}>  
                <th>Blood Group</th>  
                <th>Blood Availability</th>  
                <th colSpan="4">Action</th>  
              </tr>  
            </thead>  
            <tbody>  
             { this.blodAdminRow() }   
            </tbody>  
          </table>    
    </div>
  )
}
}