import React, { Component } from 'react';  
import axios from 'axios';

class Table extends Component {  
  constructor(props) {  
    super(props);  
    }   
    
    DeleteAvailDoc= () =>{  
        axios.delete('https://localhost:44335/Api/Doctor/DeleteDoctor?Id='+this.props.obj.id)  
       .then(json => {  
       if(json.data.Status==='Delete'){  
         alert('Doctor record deleted successfully!!');  
         window.location.reload(false);
       }  
       })  
       }  



  render() {  
    return (  
        <tr style={{ textAlign: "center" }}>  
          <td>  
            {this.props.obj.doctorName}  
          </td>  
          <td>  
            {this.props.obj.doctorSpecilaity}  
          </td>
          <td>  
            {this.props.obj.doctorAvilability}  
          </td>
          <td>  
          <button type="button" class="btn btn-danger" onClick={this.DeleteAvailDoc}>Delete</button>  
          </td> 
        </tr>  
    );  
  }  
}  

export default Table;  