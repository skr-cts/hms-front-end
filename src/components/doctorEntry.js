import React,{useState,useEffect} from "react"
import {useHistory} from 'react-router-dom'
import "./loginDoctor.css";

function DocEntryForm() {
  const[DoctorName,setName]=useState("");
  const[DoctorSpecilaity,setSpec]=useState("");
  const [DoctorAvilability, setAvail] = useState("");

    const [DoctorNameErr, setNameErr] = useState("");
    const [DoctorSpecilaityErr, setSpecErr] = useState("");
    const [DoctorAvilabilityErr, setAvailErr] = useState("");

    const history = useHistory();

    const formValidation = () => {
        const DoctorNameErr = {};
        const DocotorSpecialityErr = {};
        const DocotorAvilabilityErr = {};

        let isValid = true;

        if (!DoctorName) {
            DoctorNameErr.nullDN = "Please enter Doctor Name.";
            isValid = false;
        }
        if (!DoctorSpecilaity) {
            DocotorSpecialityErr.nullDS = "Please select Doctor's Speciality.";
            isValid = false;
        }
        if (!DoctorAvilability) {
            DocotorAvilabilityErr.nullDA = "Please select Doctor's Availability.";
            isValid = false;
        }
        setNameErr(DoctorNameErr);
        setSpecErr(DocotorSpecialityErr);
        setAvailErr(DocotorAvilabilityErr);

        return isValid;
    }

  async function docEntry()
  {
    const isValid = formValidation();
    if (isValid) {
    let item = {DoctorName,DoctorSpecilaity,DoctorAvilability}
    let result = await fetch("https://localhost:44335/Api/Doctor/AddDoctor",{
      method:'POST',
      headers:{
        "Content-Type":"application/json",
        "Accept":"application/json"
      },
      body:JSON.stringify(item)
    });
    result = await result.json();
    localStorage.setItem('docEnt',JSON.stringify(result));
    alert("Doctor Added Successfully...")
      }
      else {
          alert("Kindly fill the details correctly!!!")
      }
  }

  function handleSubmit(event){
    event.preventDefault();
  }

  return(
    <div id="doctorform">
      <form onSubmit = {handleSubmit}>
        <h2 id="headerTitle">Doctor's Here...</h2>
        <div class = 'row'>
          <label>Doctor Name</label>
                  <input type="name" onChange={(e) => setName(e.target.value)} placeholder="Ex: Dr. DocName" />
                  {Object.keys(DoctorNameErr).map((key) => {
                      return <div style={{ color: "red" }}>{DoctorNameErr[key]}</div>
                  })}
        </div>

        <div class = 'row'>
                    <label for = "spec" >Doctor Speciality:</label>
                    <select name="spec" id="spec"  onChange={(e)=>setSpec(e.target.value)}>
                        <option selected disabled hidden value="none">Select an Option</option>
                        <option value="Cardio">Cardio</option>
                        <option value="Ortho">Ortho</option>
                        <option value="Pediatric">Pediatric</option>
                        <option value="Gynaco">Gynaco</option>
                        <option value="Optho">Optho</option>
                  </select>
                  {Object.keys(DoctorSpecilaityErr).map((key) => {
                      return <div style={{ color: "red" }}>{DoctorSpecilaityErr[key]}</div>
                  })}
                    </div>

                    <div class = 'row' >
                      <label>Availability</label>
                      <select name="spec" id="spec"  onChange={(e)=>setAvail(e.target.value)}>
                        <option selected disabled hidden value="none">Select an Option</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                  </select>
                  {Object.keys(DoctorAvilabilityErr).map((key) => {
                      return <div style={{ color: "red" }}>{DoctorSpecilaityErr[key]}</div>
                  })}
                      </div>

        <div class = 'row' id = 'button'>
          <button type="submit"onClick={docEntry}>Submit</button>
        </div>
      </form>
    </div>
  )
}


//ReactDOM.render(<App />, document.getElementById('container'));
export default DocEntryForm;