import React, { Component } from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom'

class Table extends Component {

  DeleteVaccination = () => {
    axios.delete('https://localhost:44335/Api/Vaccine/DeleteVaccination/?id=' + this.props.obj.id)
      .then(json => {
        if (json.data.Status === 'Delete') {
          window.location.reload(false);
        }
      })
  }

  render() {
    return (
      <tr style={{ textAlign: "center" }}>
        <td name="email">
          {this.props.obj.userMail}
        </td>
        <td name="date">
          {this.props.obj.confrimDateOfVaccination}
        </td>
        <td>
          {this.props.obj.vaccineName}
        </td>
        <td>
          {this.props.obj.chooseSlot}
        </td>
        <td>
          {this.props.obj.idProof}
        </td>
        <td>
        <button type="button" class="btn btn-success" ><Link style={{ textDecoration: "none", color: 'white'}} to = {"/components/mailVaccination/"+this.props.obj.id} >Accept</Link></button>
        </td>
        <td>
          <button type="button" class="btn btn-danger" onClick={this.DeleteVaccination} >Delete</button>
        </td>
      </tr>
    );
  }
}

export default Table;