import React from "react"
import emailjs from "emailjs-com";
import axios from "axios";


export default class MailForm extends React.Component {
    constructor(props) {
        super(props);

        this.onChangeMail = this.onChangeMail.bind(this);
        this.onChangeDate = this.onChangeDate.bind(this);

        this.state = {
            UserMail: '',
            ConfrimDate: ''
        }
    }
    componentDidMount() {
        console.log(this.props.match.params.id);
        axios.get('https://localhost:44335/Api/Appoitment/AppointmentById/'+this.props.match.params.id)
            .then(response => {
                console.log(response.data);
                this.setState({
                    UserMail: response.data.userMail,
                    ConfrimDate: response.data.confrimdate
                });

            })
            .catch(function (error) {
                console.log(error);
            })
    }

    onChangeMail(e) {
        this.setState({
            UserMail: e.target.value
        });
    }
    onChangeDate(e) {
        this.setState({
            ConfrimDate: e.target.value
        });
    }

    sendEmail(e) {
        e.preventDefault();

        emailjs.sendForm('service_wbvmblm', 'template_9fzs1p8', e.target, 'jYyCjFqvZ3DLOKzks')
            .then((result) => {
                alert("Successfully mail sent!");
            }, (error) => {
                alert("Mail failed to send!!");
            });
        e.target.reset()
    }
    render() {
        return (
            <div id="loginform">
                <form onSubmit = {this.sendEmail}>
                    <h2 id="headerTitle">Are you sure to confirm?!</h2>
                    <div class='row'>
                        <label>EmailID</label>
                        <input style={{color:'orange'}} type="email" name="email" value={this.state.UserMail} onChange={(e) => this.onChangeMail} placeholder="Enter your email ID" />
                    </div>

                    <div class='row' >
                        <label>Date of Appointment</label>
                        <input style={{color:'orange'}} type="text" name="date" value={this.state.ConfrimDate} onChange={(e) => this.onChangeDate} placeholder="Enter your full name" />
                    </div>

                    <div>
                        <button class = "offset" id = "button2" type="submit">Send Mail</button>
                    </div>
                </form>
            </div>
        )
    }
}


//ReactDOM.render(<App />, document.getElementById('container'));