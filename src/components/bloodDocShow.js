import React, { Component } from 'react';  
import axios from 'axios';

class Table extends Component {  
  constructor(props) {  
    super(props);  
    }
    
    DeleteAvailDoc = () =>{ 

        axios.delete('https://localhost:44335/Api/Blood/DeleteBlood?id='+this.props.obj.id)  
       .then(json => {
         console.log(json)
       if(json.data.Status==='Delete'){  
         alert('Blood record deleted successfully!!');  
         window.location.reload(false);
       }  
       })  
       }  

  render() {  
    return (  
        <tr style={{ textAlign: "center" }}>  
          <td>  
            {this.props.obj.bloodGroup}  
          </td>
          <td>  
            {this.props.obj.bloodAvilability}  
          </td>
          <td>  
          <button type="button" class="btn btn-danger" onClick={this.DeleteAvailDoc}>Delete</button>  
          </td> 
        </tr>  
    );  
  }  
}  

export default Table;  