import React from 'react';
import { Link } from 'react-router-dom'
import Powerslap from "./media/Powerslap.mp4"
import './homeAndDashboard.scss'

class HomePage extends React.Component{
    render() {
        return (
            <div id='homeheaderdesign'>
                <div>
                    <video autoPlay loop muted style={{
                        width: "100%", left: "100%",
                        top: "100%", height: "100%", objectFit: "cover", transofrm: "translate(-50%,-50%)",
                        zIndex: "-1"
                    }}>
                        <source src={Powerslap} type="video/mp4" />
                    </video>
                
                    <div class="content">
                        <HomePageText title="Hospital Management System" />
                        <p>Declare the past, diagnose the present, foretell the future</p>
                        <div>
                            <FormButtonPatient title="Click here to cure you..." />
                        </div>
                        <div>
                          <Link id = "button2" class = "offset" to = '/components/Help' style={{ textDecoration: "none", color: 'black'}}>Help</Link>
                        </div>


                    </div>
                    <footer> <big>&copy; Copyright POD 3 Hospital Management System</big> </footer>

                    
                </div>
            </div>

        )
    }
}

const HomePageText = props => (
  <h1>{props.title}</h1>
);

const FormButtonPatient = props => (
    
    <div >
      <button id = "button1" class="offset">
        <Link to = "./loginPatient" style={{textDecoration:"none",color:'black'}}>
        {props.title}
        </Link>
      </button>
    </div>
);

//ReactDOM.render(<App />, document.getElementById('container'));
export default HomePage;