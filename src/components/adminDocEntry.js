import React from "react"
import axios from "axios";
import DocTable from './adminDocShow'

export default class AdminShowcase extends React.Component{
    
    constructor(props) {  
        super(props);  
        this.state = {listOfDoc: []};  
      }  
      componentDidMount(){  
        debugger;  
        axios.get('https://localhost:44335/Api/Doctor/DoctorDetails')  
          .then(response => {  console.log(response.data)
            this.setState({ listOfDoc: response.data });  
            debugger; 
  
          })  
          .catch(function (error) {  
            console.log(error);  
          })  
      }  
  
      docAdminRow(){  
        return this.state.listOfDoc.map(function(object, i){  
            return <DocTable obj={object} key={i} />;  
        });  
      }

    render(){
    return(
    <div id="showcaseform">
      <h4 id="headerTitle">Doctor Entry and Exit</h4>
          <table className="table table-striped" style={{ marginTop: 10 }}>  
            <thead>  
              <tr style={{ textAlign: "center" }}>  
                <th>Doctor Name</th>  
                <th>Doctor Speciality</th>  
                <th>Doctor Availability</th>  
                <th colSpan="4">Action</th>  
              </tr>  
            </thead>  
            <tbody>  
             { this.docAdminRow() }   
            </tbody>  
          </table>    
    </div>
  )
}
}