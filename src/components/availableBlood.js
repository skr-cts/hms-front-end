import React, { Component } from 'react';  
import axios from 'axios';

class Table extends Component {  
  constructor(props) {  
    super(props);  
    }   
    bloodAvailability(){
        axios.post('https://localhost:44335/Api/Blood/AddBlood')  
        .then(json => {  
        if(json.data.Status ==='Success'){  
          alert('Blood Entry Successful!!');
        }  
        })
    }

  render() {  
    return (  
        <tr style={{ textAlign: "center" }}>  
          {/* <td>  
            {this.props.obj.DoctorName}  
          </td>   */}
          <td>  
            {this.props.obj.BloodGroup}  
          </td>
          <td>  
            {this.props.obj.BloodAvilability}  
          </td> 
        </tr>
    );  
  }  
}  

export default Table;  