import React, { useState, useEffect } from "react"
import { useHistory } from 'react-router-dom'
import ReactCSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

function VaccinationForm() {

    const [ConfrimDateOfVaccination, setConfrimdate] = useState("");
    const [ChooseSlot, setSlot] = useState("");
    const [VaccineName, setVaccination] = useState("");
    const [IdProof, setProof] = useState("");

    const [ConfrimDateOfVaccinationErr, setConfrimdateErr] = useState("");
    const [ChooseSlotErr, setSlotErr] = useState("");
    const [VaccineNameErr, setVaccinationErr] = useState("");
    const [IdProofErr, setProofErr] = useState("");

    const history = useHistory();

    const formValidation = () => {
        const ConfrimDateOfVaccinationErr = {};
        const ChooseSlotErr = {};
        const VaccineNameErr = {};
        const IdProofErr = {};

        let isValid = true;

        if (!ConfrimDateOfVaccination) {
            ConfrimDateOfVaccinationErr.nullCDOV = "Please confirm date of vaccination.";
            isValid = false;
        }
        if (!ChooseSlot) {
            ChooseSlotErr.nullCS = "Please choose slot of vaccination.";
            isValid = false;
        }
        if (!VaccineName) {
            VaccineNameErr.nullVN = "Please choose type of vaccination.";
            isValid = false;
        }
        if (!IdProof) {
            IdProofErr.nullVN = "Please enter ID Proof.";
            isValid = false;
        }
        else if (isNaN(IdProof)) {
            IdProofErr.invalidProof = "Please enter a valid ID proof";
        }
        setConfrimdateErr(ConfrimDateOfVaccinationErr);
        setSlotErr(ChooseSlotErr);
        setVaccinationErr(VaccineNameErr);
        setProofErr(IdProofErr);

        return isValid;

    }

    function handleSubmit(event) {
        event.preventDefault();
    }

    async function vaccineDetails() {
        const isValid = formValidation();
        if (isValid) {
            let temp = sessionStorage.getItem('User');
            let UserMail = JSON.parse(temp);
            let item = { ConfrimDateOfVaccination, ChooseSlot, VaccineName, IdProof, UserMail }
            let result = await fetch("https://localhost:44335/Api/Vaccine/VaccinationRegistration", {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                body: JSON.stringify(item)
            });
            result = await result.json();
            alert("Vaccine slot request has been done!");
        }
        else {
            alert("Kindly fill the details correctly!!!")
        }
    }

    return (
        <ReactCSSTransitionGroup
            component="div"
            className="animated-time"
            transitionName="time"
            transitionAppear={true}
            transitionAppearTimeout={500}
            transitionEnterTimeout={500}
            transitionLeaveTimeout={500}
        >
            <div id="applicationForm">
                <form onSubmit={handleSubmit}>
                    <h2 id="headerTitle">Get Vaccinated!</h2>
                    <div id="leftHalfSignUp">
                        <div class='row'>
                            <label>Confirm Date of Vaccination</label>
                            <input type="date" onChange={(e) => setConfrimdate(e.target.value)} placeholder="Ex : DD/MM/YYYY" />
                            {Object.keys(ConfrimDateOfVaccinationErr).map((key) => {
                                return <div style={{ color: "red" }}>{ConfrimDateOfVaccinationErr[key]}</div>
                            })}
                        </div>

                        <div class='row'>
                            <label for="spec" >Choose Slot</label>
                            <select name="spec" id="spec" onChange={(e) => setSlot(e.target.value)}>
                                <option selected disabled hidden value="none">Select an Option</option>
                                <option value="Slot1 - 10AM TO 11AM">1 - 10AM TO 11AM</option>
                                <option value="Slot2 - 11AM TO 12PM">2 - 11AM TO 12PM</option>
                                <option value="Slot3 - 12PM TO 1PM">3 - 12PM TO 1PM</option>
                            </select>
                            {Object.keys(ChooseSlotErr).map((key) => {
                                return <div style={{ color: "red" }}>{ChooseSlotErr[key]}</div>
                            })}
                        </div>
                    </div>

                    <div id="rightHalfSignUp">
                        <div class='row'>
                            <label for="spec" >Vaccination Name</label>
                            <select name="spec" id="spec" onChange={(e) => setVaccination(e.target.value)}>
                                <option selected disabled hidden value="none">Select Vaccine</option>
                                <option value="CoviShield">CoviShield</option>
                                <option value="Co-Vaccine">Co-Vaccine</option>
                                <option value="Sputnik">Sputnik</option>
                            </select>
                            {Object.keys(VaccineNameErr).map((key) => {
                                return <div style={{ color: "red" }}>{VaccineNameErr[key]}</div>
                            })}
                        </div>

                        <div class='row'>
                            <label>ID Proof</label>
                            <input type="text" onChange={(e) => setProof(e.target.value)} placeholder="Aadhar preferable" />
                            {Object.keys(IdProofErr).map((key) => {
                                return <div style={{ color: "red" }}>{IdProofErr[key]}</div>
                            })}
                        </div>
                    </div>

                    <div class='rowCore' id='button'>
                        <button type="submit" onClick={vaccineDetails}><b>Book Vaccination</b></button>
                    </div>
                </form>
            </div>
        </ReactCSSTransitionGroup>
    )
}


//ReactDOM.render(<App />, document.getElementById('container'));
export default VaccinationForm;