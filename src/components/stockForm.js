import React, { useState, useEffect } from "react"
import { useHistory } from 'react-router-dom'
import "./loginDoctor.css";

function DocEntryForm() {
    const [AvailVaccineName, setVacName] = useState("");
    const [Limit, setVacLimit] = useState("");

    const [AvailVaccineNameErr, setVacNameErr] = useState("");
    const [LimitErr, setVacLimitErr] = useState("");

    const history = useHistory();

    const formValidation = () => {
        const AvailVaccineNameErr = {};
        const LimitErr = {};

        let isValid = true;

        if (!AvailVaccineName) {
            AvailVaccineNameErr.nullDN = "Please enter Doctor Name.";
            isValid = false;
        }
        if (!Limit) {
            LimitErr.nullDS = "Please select Doctor's Speciality.";
            isValid = false;
        }
        setVacNameErr(AvailVaccineNameErr);
        setVacLimitErr(LimitErr);

        return isValid;
    }

    async function stockEntry() {
        const isValid = formValidation();
        if (isValid) {
            let item = { AvailVaccineName, Limit }
            let result = await fetch("https://localhost:44335/Api/VaccineList/AddLimit", {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                    
                },
                body: JSON.stringify(item)
                
            });
            result = await result.json();
            localStorage.setItem('docEnt', JSON.stringify(result));
            alert("Covid-19 Vaccine Added Successfully...")
        }
        else {
            alert("Kindly fill the details correctly...")
        }
    }

    return (
        <div id="stockform">
            <form>
                <h2 id="headerTitle">Add Stock</h2>
                <div class='row'>
                <label for="vaccine" >Vaccine Name:</label>
                    <select name="spec" id="spec" onChange={(e) => setVacName(e.target.value)}>
                                <option selected disabled hidden value="none">Select a Vaccine</option>
                                <option value="CoviShield">CoviShield</option>
                                <option value="Covaxin">Covaxin</option>
                                <option value="Sputnik V">Sputnik V</option>
                                <option value="Astrageneca">Astrageneca</option>
                                <option value="Moderna">Moderna</option>
                            </select>
                    {Object.keys(AvailVaccineNameErr).map((key) => {
                        return <div style={{ color: "red" }}>{AvailVaccineNameErr[key]}</div>
                    })}
                </div>

                <div class='row'>
                    <label>Vaccine Quantity</label>
                    <input type="number" min="0" onChange={(e) => setVacLimit(e.target.value)} placeholder="Enter vaccine limit" />
                    {Object.keys(AvailVaccineNameErr).map((key) => {
                        return <div style={{ color: "red" }}>{AvailVaccineNameErr[key]}</div>
                    })}
                </div>

                <div class='row' id='button'>
                    <button type="submit" onClick={stockEntry}>Submit</button>
                </div>
            </form>
        </div>
    )
}


//ReactDOM.render(<App />, document.getElementById('container'));
export default DocEntryForm;