import "./signupDoctor.css";
import React,{useState, useEffect} from "react"
import {useHistory} from 'react-router-dom'


function SignUpFormDoctor(){
  const[Name,setName] =useState("");
  const[UserName,setUName]=useState("");
  const[HighestDegree,setDeg]=useState("");
  const[Age,setAge]=useState("");
  const[Speciality,setSpec]=useState("");
  const[Email,setEmail]=useState("");
  const[Password,setPassword]=useState("");
    
  const history=useHistory();
  useEffect(()=>{
    if(localStorage.getItem('user-info')){
      history.push("/components/home")
    }
  },[]);
  
  async function signUp()
  {
    let item={Name,UserName,HighestDegree,Age,Speciality,Email,Password}
    let result = await fetch("https://localhost:44335/Api/login/InsertDoctor",{
        method:'POST',
        headers:{
            "Content-Type":"application/json",
            "Accept":"application/json"
        },
        body:JSON.stringify(item)
    });
    result = await result.json();
    localStorage.setItem('user-info',JSON.stringify(result));
    var checkSignUp = JSON.parse(localStorage.getItem('user-info'));
    if(expect(checkSignUp.value).toEqual("null")){
      window.alert("Invalid User");
      history.push('/components/signupDoctor');
    }
    history.push("/components/home");
  }

  return(
    <div id="signupform">
      <form>
      <h2 id="headerTitle">Signing you up, Doc!</h2>
      <div id = "leftHalfSignUp">
        <div class = 'row'>
          <label>Name</label>
          <input type="text" onChange={(e)=>setName(e.target.value)} placeholder="Enter your full name" />
        </div>

        <div class = 'row'>
          <label>User Name</label>
          <input type="text" onChange={(e)=>setUName(e.target.value)} placeholder="Enter your user name" />
        </div>

        <div class = 'row' >
          <label>Highest Degree</label>
          <input type="text" onChange={(e)=>setDeg(e.target.value)} placeholder="Enter your highest degree" />
        </div>

        <div class = 'row' >
          <label>Age</label>
          <input type="text" onChange={(e)=>setAge(e.target.value)} placeholder="Enter your age" />
        </div>
      </div>
        
      <div id = "rightHalfSignUp">


        <div class = 'row'>
          <label>Speciality</label>
          <input type="text" onChange={(e)=>setSpec(e.target.value)} placeholder="Enter your field speciality" />
        </div>

        <div class = 'row'>
          <label>EmailID</label>
          <input type="text" onChange={(e)=>setEmail(e.target.value)} placeholder="Enter your mail ID" />
        </div>

        <div class = 'row' >
          <label>Password</label>
          <input type="password" onChange={(e)=>setPassword(e.target.value)} placeholder="Enter your password" />
        </div>
      </div>

      <div id = 'registerButton'>
        <div id="button" onClick={signUp} class="row">
          <button>Register</button>
        </div>
      </div>
      </form>
    </div>
  )
}

// ReactDOM.render(<App />, document.getElementById('container'));
export default SignUpFormDoctor;