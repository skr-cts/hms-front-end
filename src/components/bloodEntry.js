import React,{useState,useEffect} from "react"
import {useHistory} from 'react-router-dom'
import "./loginBlood.css";

function BlodEntryForm() {
  // const[BloodGroup,setName]=useState("");
  const[BloodGroup,setSpec]=useState("");
  const [BloodAvilability, setAvail] = useState("");

    // const [BloodGroupErr, setNameErr] = useState("");
    const [BloodGroupErr, setNameErr] = useState("");
    const [BloodAvilabilityErr, setAvailErr] = useState("");

    const history = useHistory();

    const formValidation = () => {
        // const BloodNameErr = {};
        const BloodGroupErr = {};
        const BloodAvilabilityErr = {};

        let isValid = true;

        // if (!BloodGroup) {
        //     BloodNameErr.nullDN = "Please enter Blood Name.";
        //     isValid = false;
        // }
        if (!BloodGroup) {
          BloodGroupErr.nullDS = "Please select Blood Group.";
          isValid = false;
      }
        if (!BloodAvilability) {
            BloodAvilabilityErr.nullDA = "Please select Blood Availability.";
            isValid = false;
        }
        setNameErr(BloodGroupErr);
        setAvailErr(BloodAvilabilityErr);

        return isValid;
    }

  async function blodEntry()
  {
    const isValid = formValidation();
    if (isValid) {
    let item = {BloodGroup,BloodAvilability}
    let result = await fetch("https://localhost:44335/Api/Blood/AddBlood",{
      method:'POST',
      headers:{
        "Content-Type":"application/json",
        "Accept":"application/json"
      },
      
      body:JSON.stringify(item)
    });
    result = await result.json();
    localStorage.setItem('blodEnt',JSON.stringify(result));
    alert("Blood Added Successfully...")
      }
      else {
          alert("Kindly fill the details correctly!!!")
      }
  }

  function handleSubmit(event){
    event.preventDefault();
  }

  return(
    <div id="Bloodform">
      <center><form onSubmit = {handleSubmit}>
      <br></br>
          <br></br>
          <br></br>
          <br></br>
        <h2 id="headerTitle">Add Blood...</h2>
        {/* <div class = 'row'>
          <label>Blood Group</label>
                  <input type="name" onChange={(e) => setName(e.target.value)} placeholder=" BloodGroup" />
                  {Object.keys(BloodGroupErr).map((key) => {
                      return <div style={{ color: "red" }}>{BloodGroupErr[key]}</div>
                  })}
        </div> */}

        <div class = 'row'>

                    <label for = "spec" >Blood Group:</label>
                    <select name="spec" id="spec"  onChange={(e)=>setSpec(e.target.value)}>
                        <option selected disabled hidden value="none">Select an Option</option>
                        <option value="A+">A+</option>
                        <option value="A">A</option>
                        <option value="AB+">AB+</option>
                        <option value="AB-">AB-</option>
                        <option value="B+">B+</option>
                        <option value="B">B</option>
                        <option value="O+">O+</option>
                        <option value="O-">O-</option>
                  </select>
                  {Object.keys(BloodGroupErr).map((key) => {
                      return <div style={{ color: "red" }}>{BloodGroupErr[key]}</div>
                  })}
                    </div>

                    <div class = 'row' >
                      <label>Availability:</label>
                      <select name="spec" id="spec"  onChange={(e)=>setAvail(e.target.value)}>
                        <option selected disabled hidden value="none">Select an Option</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                  </select>
                  {Object.keys(BloodAvilabilityErr).map((key) => {
                      return <div style={{ color: "red" }}>{BloodGroupErr[key]}</div>
                  })}
                      </div>

        <div class = 'row' id = 'button'>
          <button type="submit"onClick={blodEntry}>Submit</button>
        </div>
      </form></center>
    </div>
  )
}


//ReactDOM.render(<App />, document.getElementById('container'));
export default BlodEntryForm;