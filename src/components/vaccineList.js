import React, { Component } from 'react';  
import axios from 'axios';  
import { Link } from 'react-router-dom';  

class Table extends Component {  
  constructor(props) {  
    super(props);  
    }  
  render() {  
    return (  
        <tr style={{ textAlign: "center" }}>  
          <td>  
            {this.props.obj.availVaccineName}  
          </td>  

          <td>  
            {this.props.obj.limit}  
          </td>  
        </tr>  
    );  
  }  
}  

export default Table;  