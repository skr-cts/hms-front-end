import React, { useState, useEffect } from "react"
import { useHistory } from 'react-router-dom'
import ReactCSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

function ApplicationForm() {

    const [Confrimdate, setConfrimdate] = useState("");
    const [Doctortype, setDoctortype] = useState("");
    const [Problemdescription, setProblemdescription] = useState("");
    const [Counseledbefore, setCounseledbefore] = useState("");

    const [ConfrimdateErr, setConfrimdateErr] = useState("");
    const [DoctortypeErr, setDoctortypeErr] = useState("");
    const [ProblemdescriptionErr, setProblemdescriptionErr] = useState("");
    const [CounseledbeforeErr, setCounseledbeforeErr] = useState("");

    const history = useHistory();

    const formValidation = () => {
        const ConfirmdateErr = {};
        const DoctortypeErr = {};
        const ProblemdescriptionErr = {};
        const CounseledbeforeErr = {};

        let isValid = true;

        if (!Confrimdate) {
            ConfirmdateErr.nullCD = "please confirm date of appointment."
            isValid = false;
        }
        if (!Doctortype) {

            DoctortypeErr.nullDT = "please select type of Doctor required."
            isValid = false;
        }
        if (!Problemdescription) {
            ProblemdescriptionErr.nullPD = "please enter the problem description."
            isValid = false;
        }
        if (!Counseledbefore) {
            CounseledbeforeErr.nullCB = "please enter YES/NO."
            isValid = false;
        }
        setConfrimdateErr(ConfirmdateErr);
        setDoctortypeErr(DoctortypeErr);
        setProblemdescriptionErr(ProblemdescriptionErr);
        setCounseledbeforeErr(CounseledbeforeErr);

        return isValid;

    }

    function handleSubmit(event){
        event.preventDefault();
      }

    async function signUp() {
        const isValid = formValidation();
        if (isValid) {
            let temp = sessionStorage.getItem('User');
            let UserMail = JSON.parse(temp);
            let item = { Confrimdate, Doctortype, Problemdescription, Counseledbefore, UserMail }
            let result = await fetch("https://localhost:44335/Api/Appoitment/Addappoitment", {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                body: JSON.stringify(item)
            });
            result = await result.json();
            alert("Appointment request has been done!");
        }
        else {
            alert("Kindly fill the details correctly!!!")
        }
    }
    return (
        <ReactCSSTransitionGroup
            component="div"
            className="animated-time"
            transitionName="time"
            transitionAppear={true}
            transitionAppearTimeout={500}
            transitionEnterTimeout={500}
            transitionLeaveTimeout={500}
        >
            <div id="applicationForm">
                <form onSubmit = {handleSubmit}>
                    <h2 id="headerTitle">Get your Appointment!</h2>
                    <div id="leftHalfSignUp">
                        <div class='row'>
                            <label>Confirm Date of Appointment</label>
                            <input type="date" onChange={(e) => setConfrimdate(e.target.value)} placeholder="Ex : DD/MM/YYYY" />
                            {Object.keys(ConfrimdateErr).map((key) => {
                                return <div style={{ color: "red" }}>{ConfrimdateErr[key]}</div>
                            })}
                        </div>

                        <div class='row'>
                            <label for="spec" >Doctor Speciality:</label>
                            <select name="spec" id="spec" onChange={(e) => setDoctortype(e.target.value)}>
                                <option selected disabled hidden value="none">Select an Option</option>
                                <option value="Cardio">Cardio</option>
                                <option value="Ortho">Ortho</option>
                                <option value="Pediatric">Pediatric</option>
                                <option value="Gynaco">Gynaco</option>
                                <option value="Optho">Optho</option>
                            </select>
                            {Object.keys(DoctortypeErr).map((key) => {
                                return <div style={{ color: "red" }}>{DoctortypeErr[key]}</div>
                            })}
                        </div>
                    </div>

                    <div id="rightHalfSignUp">
                        <div class='row'>
                            <label>Problem Description</label>
                            <input type="text" onChange={(e) => setProblemdescription(e.target.value)} placeholder="Give a small descriptions about problem" />
                            {Object.keys(ProblemdescriptionErr).map((key) => {
                                return <div style={{ color: "red" }}>{ProblemdescriptionErr[key]}</div>
                            })}
                        </div>

                        <div class='row'>
                        <label for="counc" >Counseled Before?</label>
                        <select name="counc" id="counc" onChange={(e) => setCounseledbefore(e.target.value)}>
                            <option selected disabled hidden value="none">Select an Option</option>
                                <option value="YES">YES</option>
                                <option value="NO">NO</option>
                                </select>
                            {Object.keys(CounseledbeforeErr).map((key) => {
                                return <div style={{ color: "red" }}>{CounseledbeforeErr[key]}</div>
                            })}
                        </div>
                    </div>

                    <div class='rowCore' id='button'>
                        <button type="submit" onClick={signUp}><b>Create Appointment</b></button>
                    </div>
                </form>
            </div>
        </ReactCSSTransitionGroup>
    )
}


//ReactDOM.render(<App />, document.getElementById('container'));
export default ApplicationForm;