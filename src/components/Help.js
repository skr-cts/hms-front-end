import React,{useState,useEffect} from "react"
import {useHistory} from 'react-router-dom'

import emailjs from "emailjs-com";


function LoginFormPatient() {
    const [Email, setEmail] = useState("");
    const [Name, setName] = useState("");
    const [DriverSpecification, setDriverSpecification] = useState("");

    const [EmailErr, setEmailErr] = useState("");
    const [NameErr, setNameErr] = useState("");
    const [DriverSpecificationErr, setDriverSpecificationErr] = useState("");

    const history = useHistory();

    const formValidation = () => {
        const EmailErr = {};
        const NameErr = {};
        const DriverSpecificationErr = {};

        let isValid = true;

        if (!Email) {
            EmailErr.nullEmail = "Email id is required";
            isValid = false;
        }
        else if (!(/^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w{2,3})+$/.test(Email))) {
            EmailErr.invalidEmail = "Invalid EmailID";
            isValid = false;
        }
        if (!Name) {
            NameErr.nullN = "Name is required";
            isValid = false;
        }
        if (!DriverSpecification) {
            DriverSpecificationErr.nullDS = "Please choose DriverSpecification";
            isValid = false;
        }

        setEmailErr(EmailErr);
        setNameErr(NameErr);
        setDriverSpecificationErr(DriverSpecificationErr);

        return isValid;
    }
    function sendEmail(e) {
        const isValid = formValidation();
        if (isValid) {
          e.preventDefault();

          emailjs.sendForm('service_fiqc5on', 'template_2ai8fg8', e.target, 'user_UQtDGF1MobWZAQQnqmjvU')
            .then((result) => {
              alert("Successfully mail sent!");
            }, (error) => {
              alert("Successfully mail sent!");
            });
          e.target.reset()
        }
        else{
          alert("Kindly fill out the fields clearly!");
        }
    }
  return(
    <div id="helpform">
      <form onSubmit = {sendEmail}>
        <h2 id="headerTitle">Let's Help You!</h2>
        <div class = 'row'>
                  <label>EmailID</label>
                  <input type="email" name = "email" value={Email} onChange={(e) => setEmail(e.target.value)} placeholder="Enter your email ID" />
                  {Object.keys(EmailErr).map((key) => {
                      return <div style={{ color: "red" }}>{EmailErr[key]}</div>
                  })}
        </div>

        <div class = 'row' >
                  <label>Full Name</label>
                  <input type="text" name = "name" value={Name} onChange={(e) => setName(e.target.value)} placeholder="Enter your full name" />
                  {Object.keys(NameErr).map((key) => {
                      return <div style={{ color: "red" }}>{NameErr[key]}</div>
                  })}
        </div>

        <div class = 'row'>
                    <label for = "spec" >Drive Specification:</label>
                  <select name="spec" id="spec" onChange={(e) => setDriverSpecification(e.target.value)} >
                        <option selected disabled hidden value="none">Select an Option</option>
                        <option value="Appointment">Appointment</option>
                        <option value="Vaccination">Vaccination</option>
                  </select>
                  {Object.keys(DriverSpecificationErr).map((key) => {
                      return <div style={{ color: "red" }}>{DriverSpecificationErr[key]}</div>
                  })}
                    </div>

        <div class = 'row' >
          <label>Issues you have faced</label>
          <textarea name = "issue" style = {{width: 380}} type="text" placeholder="Kindly tell us the issue" />
        </div>

        <div class = 'row' id = 'button'>
          <button type="submit">Submit</button>
        </div>
      </form>
    </div>
  )
}


//ReactDOM.render(<App />, document.getElementById('container'));
export default LoginFormPatient;